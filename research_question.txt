Group: 7com1079-group160


Question
=========================

RQ: Is there a significant proportional difference in win rate between home and away teams?
(in fifa international tournaments within the last century)


Null hypothesis: There is a significant proportional difference in win rate between the home
and away teams


Alternative hypothesis: There is no significant proportional difference in win rate between the home 
and away teams


Dataset
=========================

URL: https://www.kaggle.com/martj42/international-football-results-from-1872-to-2017


Column Headings:

````````````

> colnames(results)

"date"   "home_team"  "away_team"  "home_score" "away_score" "tournament" "city"  "country"

````````````
