library(tidyverse)

results=read.csv("results.csv")

# remove draw matches
results=subset(results,home_score != away_score)

results$win=ifelse(results$home_score > results$away_score,"Home","Away")

##############################################################################

table(results$win)
p1=ggplot(results) + geom_bar(mapping = aes(x = win))
p1
pdf("only.pdf")
print(p1)
dev.off()
###############################################################################

results$date=as.character(results$date)
for(i in 1:nrow(results))
{
  results$year[i]=unlist(strsplit(results$date[i],split = "-"))[1]
}

results$year=as.numeric(results$year)
res=subset(results,year>=1900)


res1=res[,c("year","win")]
res1$win=ifelse(res1$win=="Home",1,0)

#################################################################################

group1=subset(res1,year>=1900 & year<1910)
group2=subset(res1,year>=1910 & year<1920)
group3=subset(res1,year>=1920 & year<1930)
group4=subset(res1,year>=1930 & year<1940)
group5=subset(res1,year>=1940 & year<1950)
group6=subset(res1,year>=1950 & year<1960)
group7=subset(res1,year>=1960 & year<1970)
group8=subset(res1,year>=1970 & year<1980)
group9=subset(res1,year>=1980 & year<1990)
group10=subset(res1,year>=1990 & year<2000)
group11=subset(res1,year>=2000 & year<2010)
group12=subset(res1,year>=2010 & year<=2020)

Year=c(1910,1910)
Win.Rate=c("Home","Away")
Value=c(sum(group1$win==1),sum(group1$win==0))
df=data.frame(Year,Win.Rate,Value)

df=rbind(df,c(1920,"Home",sum(group2$win==1)))
df=rbind(df,c(1920,"Away",sum(group2$win==0)))
df=rbind(df,c(1930,"Home",sum(group3$win==1)))
df=rbind(df,c(1930,"Away",sum(group3$win==0)))
df=rbind(df,c(1940,"Home",sum(group4$win==1)))
df=rbind(df,c(1940,"Away",sum(group4$win==0)))
df=rbind(df,c(1950,"Home",sum(group5$win==1)))
df=rbind(df,c(1950,"Away",sum(group5$win==0)))
df=rbind(df,c(1960,"Home",sum(group6$win==1)))
df=rbind(df,c(1960,"Away",sum(group6$win==0)))
df=rbind(df,c(1970,"Home",sum(group7$win==1)))
df=rbind(df,c(1970,"Away",sum(group7$win==0)))
df=rbind(df,c(1980,"Home",sum(group8$win==1)))
df=rbind(df,c(1980,"Away",sum(group8$win==0)))
df=rbind(df,c(1990,"Home",sum(group9$win==1)))
df=rbind(df,c(1990,"Away",sum(group9$win==0)))
df=rbind(df,c(2000,"Home",sum(group10$win==1)))
df=rbind(df,c(2000,"Away",sum(group10$win==0)))
df=rbind(df,c(2010,"Home",sum(group11$win==1)))
df=rbind(df,c(2010,"Away",sum(group11$win==0)))
df=rbind(df,c(2020,"Home",sum(group12$win==1)))
df=rbind(df,c(2020,"Away",sum(group12$win==0)))

df$Value=as.numeric(df$Value)
p=ggplot(df, aes(fill=Win.Rate, y=Value, x=Year)) + 
  geom_bar(position="dodge", stat="identity")+coord_flip()+ylab("Win Rate")+xlab("Year")+
  ggtitle("Bar plot showing showing win rate of both home and away team for \n FIFA 
          international football games from 1920 to 2020")

p

pdf("All.pdf")
print(p)
dev.off()

##################################################################################

#proportion test
## Home
prop.table(table(res1$win))

nrow(res1)

# H0: P_home <= 0.6 vs H1: P_home > 0.6
p0 = 0.6
p = 0.63

ts=(p-p0)/sqrt(p0*(1-p0)/nrow(res1))

qnorm(0.95)

# *Since ts > 1.644, we reject* H0


## Away
#H_0: P_{away} >= 0.4 vs $H1: P_away < 0.4

p0 = 0.4
p = 0.37

ts=(p-p0)/sqrt(p0*(1-p0)/nrow(res1))
ts

qnorm(1-0.95)

# *Since ts < - 1.644, we reject* H0

df=as.data.frame(prop.table(table(res1$win)))
df=cbind(df,Place=c("Away","Home"))
ggplot(df,aes(x=Place,y=Freq))+geom_bar(stat="identity")+
  labs(title = "Comparatative Bar Plot for win percentage of Home and Away",
       y="Percntage",x="Place")

## Conclusion
# *According to the two hypothesis tests we can say, when a team play in their home country the win rate 
# is higher 60%. Also we can confirm that using  the bar plot also.*



  